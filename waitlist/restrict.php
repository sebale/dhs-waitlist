<?php
require('../../config.php');
require_once($CFG->libdir. '/coursecatlib.php');

/*$CFG->debug = 38911; 
$CFG->debugdisplay = true;*/

require_once('restrict_form.php');
require_once("$CFG->dirroot/enrol/waitlist/library.php");
require_once("$CFG->dirroot/enrol/renderer.php");

$id		  = required_param('id', PARAM_INT);

$course = $DB->get_record('course', array('id'=>$id), '*', MUST_EXIST);
$context = context_course::instance($course->id);

$res = $DB->get_record_sql("select id from ".$CFG->prefix."enrol where courseid=".$id." and enrol='waitlist'");
$instance = $res->id;

if ($course->id == SITEID) {
    redirect(new moodle_url('/'));
}

$return = new moodle_url('/enrol/instances.php', array('id'=>$course->id));
if (!enrol_is_enabled('waitlist')) {
    redirect($return);
}

$records = array(); $crecords = array();
$restrict_records = $DB->get_records('course_restrict_access', array('courseparent'=>$course->id));
if (count($restrict_records)){
    foreach($restrict_records as $record){
        $crecords[$record->coursechild] = 1;
    }
}

$allcategories = coursecat::make_categories_list();
$catcourses = array();
$allcourses = coursecat::get(0)->get_courses(array('recursive' => true));
if (count($allcourses)){
    foreach ($allcourses as $ccourse){
        $catcourses[$ccourse->category][$ccourse->id] = $ccourse;
        if (isset($crecords[$ccourse->id])){
            $records[$ccourse->category][] = $ccourse->id;
        }
    }
}

require_login($course);
require_capability('enrol/waitlist:config', $context);
$PAGE->set_pagelayout('admin');

$mform = new restrict_form(NULL, array($course->id, $allcategories, $catcourses, $records));
if ($mform->is_cancelled()) {
    redirect($return);
} else if ($data = $mform->get_data()) {
    $DB->delete_records('course_restrict_access', array('courseparent'=>$course->id));
    if (count($data->courseids)){
        foreach($data->courseids as $courses_arr){
            if (count($courses_arr)){
                foreach ($courses_arr as $courseid){
                    $new_parent_record = new stdClass();
                    $new_parent_record->courseparent = $course->id;
                    $new_parent_record->coursechild = $courseid;
                    $new_parent_record->timecreated = time();
                    $DB->insert_record('course_restrict_access', $new_parent_record);
                }
            }
        }
    }
    
    redirect($return);
}

$PAGE->set_url('/enrol/waitlist/restrict.php', array('id'=>$id));
navigation_node::override_active_url(new moodle_url('/enrol/waitlist/restrict.php', array('id' => $id)));

$PAGE->set_title($PAGE->course->fullname);
$PAGE->set_heading($PAGE->title);

echo $OUTPUT->header();

echo $OUTPUT->heading(get_string('enrollment_dependencies', 'enrol_waitlist'));

$mform->display();

echo $OUTPUT->footer();

