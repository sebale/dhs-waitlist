<?php

class wlNotifications {
    protected $_id;
    protected $_params;
    
    /**
     * Form definition.
     */
    function __construct($id = 0, $params = array()) {
        $this->_id = $id;
        $this->_params = $params;
    }
    
    function rebuild($id = 0, $params = array()) {
        $this->_id = $id;
        $this->_params = $params;
    }

    function process(){
        global $DB, $CFG, $USER;
        $params = (object)$this->_params;
        if ($this->_id){
            $id = $this->_id;
            $notification = $DB->get_record('waitlist_notifications', array('id'=>$id));
            $site = get_site();
            $supportuser = core_user::get_support_user();
            $message = new stdClass();
            if ($notification->id and $notification->status > 0){
                switch ($id) {
                    case 1 : 
                        if (isset($params->item)){
                            $item = $params->item;
                            $message->params = array('recipient name'=>$item->student, 'recipient email'=>$item->email, 'support user name'=>$supportuser->firstname.' '.$supportuser->lastname, 'support email'=>$supportuser->email, 'course name'=>$item->coursename, 'course link'=>$item->courselink, 'course start date'=>$item->coursestartdate);
                            if (filter_var($item->email, FILTER_VALIDATE_EMAIL)) {
                                $message->to = $DB->get_record('user', array('id'=>$item->userid));
                                $message->to->email = 'rubensauf@gmail.com';
                                $message->from = $supportuser;
                                $message->subject = $this->generate_text($notification->subject, $message->params, $notification->tags);
                                $message->message = $this->generate_text($notification->body, $message->params, $notification->tags);
                                email_to_user($message->to, $message->from, $message->subject, $message->message, $message->message);
                                $this->insert_log(array('notid'=>$id, 'userid'=>$item->userid, 'dataid'=>$item->courseid, 'datatype'=>'course'));
                            }
                        }
                        break;
                    case 2 : 
                        if (isset($params->item)){
                            $item = $params->item;
                            $message->params = array('recipient name'=>$item->student, 'recipient email'=>$item->email, 'support user name'=>$supportuser->firstname.' '.$supportuser->lastname, 'support email'=>$supportuser->email, 'course name'=>$item->coursename, 'course link'=>$item->courselink, 'course start date'=>$item->coursestartdate);
                            if (filter_var($item->email, FILTER_VALIDATE_EMAIL)) {
                                $message->to = $DB->get_record('user', array('id'=>$item->userid));
                                $message->to->email = 'rubensauf@gmail.com';
                                //$message->to->email = 'info@sebale.net';
                                $message->from = $supportuser;
                                $message->subject = $this->generate_text($notification->subject, $message->params, $notification->tags);
                                $message->message = $this->generate_text($notification->body, $message->params, $notification->tags);
                                email_to_user($message->to, $message->from, $message->subject, $message->message, $message->message);
                                $this->insert_log(array('notid'=>$id, 'userid'=>$item->userid, 'dataid'=>$item->courseid, 'datatype'=>'course'));
                            }
                        }
                        break;
                    case 3 : 
                        if (isset($params->item)){
                            $item = $params->item;
                            $message->params = array('recipient name'=>$item->student, 'recipient email'=>$item->email, 'support user name'=>$supportuser->firstname.' '.$supportuser->lastname, 'support email'=>$supportuser->email, 'course name'=>$item->coursename, 'course link'=>$item->courselink, 'course start date'=>$item->coursestartdate, 'unenroll date'=>$item->unenrolldate);
                            if (filter_var($item->email, FILTER_VALIDATE_EMAIL)) {
                                $message->to = $DB->get_record('user', array('id'=>$item->userid));
                                $message->to->email = 'rubensauf@gmail.com';
                                $message->from = $supportuser;
                                $message->subject = $this->generate_text($notification->subject, $message->params, $notification->tags);
                                $message->message = $this->generate_text($notification->body, $message->params, $notification->tags);
                                email_to_user($message->to, $message->from, $message->subject, $message->message, $message->message);
                                $this->insert_log(array('notid'=>$id, 'userid'=>$item->userid, 'dataid'=>$item->courseid, 'datatype'=>'course'));
                            }
                        }
                        break;
                    case 4 : 
                        if (isset($params->item)){
                            $item = $params->item;
                            $message->params = array('recipient name'=>$item->student, 'recipient email'=>$item->email, 'support user name'=>$supportuser->firstname.' '.$supportuser->lastname, 'support email'=>$supportuser->email, 'course name'=>$item->coursename, 'course link'=>$item->courselink, 'course start date'=>$item->coursestartdate);                            
                            if (filter_var($item->email, FILTER_VALIDATE_EMAIL)) {
                                $message->to = $DB->get_record('user', array('id'=>$item->userid));
                                $message->to->email = 'rubensauf@gmail.com';
                                $message->from = $supportuser;
                                $message->subject = $this->generate_text($notification->subject, $message->params, $notification->tags);
                                $message->message = $this->generate_text($notification->body, $message->params, $notification->tags);
                                $result = email_to_user($message->to, $message->from, $message->subject, $message->message, $message->message);
                                $this->insert_log(array('notid'=>$id, 'userid'=>$item->userid, 'dataid'=>$item->courseid, 'datatype'=>'course'));
                            }
                        }
                        break;
                }
            }
        }
    }
    
    function generate_text($text = '', $params = array(), $tags){
        $message = '';
        if ($text == '') return $message;
        $tags = unserialize($tags);
        if (count($tags) > 0){
            $message = $text;
            foreach ($tags as $tag){
                if (strripos($message, '['.$tag.']') !== FALSE and isset($params[$tag])){
                    $message = str_replace('['.$tag.']', $params[$tag], $message);
                }
            }
        }
        return $message;
    }
    
    function insert_log($params){
        global $DB;
        if (count($params)){
            $log = (object)$params;
            $log->timesend = time();
            $log->id = $DB->insert_record('waitlist_notifications_logs', $log);
        }
    }

}

?>

