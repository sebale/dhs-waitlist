<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/formslib.php');

class restrict_form extends moodleform {

    function definition() {
        global $CFG, $OUTPUT;
        $mform = $this->_form;

        list($id, $categories, $catcourses, $records) = $this->_customdata;
        
        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);
        $mform->setDefault('id', $id);
        
        if (count($catcourses)){
            foreach($categories as $categoryid=>$categoryname){
                if (!isset($catcourses[$categoryid])) continue;
                $mform->addElement('header', 'header_'.$categoryid, $categoryname);
                
                $options = array();
                foreach ($catcourses[$categoryid] as $course) {
                    if ($course->id == $id) continue;
                    $options[$course->id] = $course->fullname;
                }
                $mform->addElement('select', 'courseids['.$categoryid.']', get_string('courses'), $options);
                $mform->getElement('courseids['.$categoryid.']')->setMultiple(true);
                $mform->setType(PARAM_INT);
                if (isset($records[$categoryid]) and count($records[$categoryid])){
                    $mform->getElement('courseids['.$categoryid.']')->setSelected($records[$categoryid]);
                }
            }
        }
        
        $this->add_action_buttons(true, get_string('save', 'enrol_waitlist'));

    }

    function validation($data, $files) {
        global $DB, $CFG;
        $errors = parent::validation($data, $files);

        return $errors;
    }
}