<?php

/**
 * *************************************************************************
 * *                  Waitlist Enrol                                      **
 * *************************************************************************
 * @copyright   emeneo.com                                                **
 * @link        emeneo.com                                                **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************
*/
defined('MOODLE_INTERNAL') || die();

require_once("$CFG->libdir/formslib.php");

class enrol_waitlist_enrol_form extends moodleform {
    protected $instance;

    public function definition() {
        $mform = $this->_form;
        $instance = $this->_customdata;
        $this->instance = $instance;
        $plugin = enrol_get_plugin('waitlist');

        if ($instance->password) {
            $heading = $plugin->get_instance_name($instance);
            $mform->addElement('header', 'waitlistheader', $heading);
            $mform->addElement('passwordunmask', 'enrolpassword', get_string('password', 'enrol_waitlist'));
        } else {
            // nothing?
        }
		//echo "<pre>";print_r($instance);die();
		$currentTime = time();
		$isDisabled = false;
		$openTime = $closeTime = 0;
		if($instance->enrolstartdate)$openTime = $instance->enrolstartdate;
		if($instance->enrolenddate)$closeTime = $instance->enrolenddate;
		if($openTime&&($currentTime<$openTime)){
			$isDisabled = true;
		}
		
		if($closeTime&&($currentTime>$closeTime)){
			$isDisabled = true;
		}

        global $USER;
        if($instance->customchar1){
            if($USER->phone2 != strtoupper($instance->customchar1)){
                 $isDisabled = true;
            }

            if($instance->customchar2){
                $v1 = $USER->department;
                $v1 = substr($v1,1,(strlen($v1)-1));
                $v1 = substr($v1,0,-1);
                 if($v1 != strtoupper($instance->customchar2)){
                    $isDisabled = true;
                 }
            }
        }
		if($isDisabled){
			$mform->addElement('html', get_string('disable', 'enrol_waitlist'));
			$mform->addElement("html","<br/><br/><p align='center'><input type='button' value='".get_string('continue', 'enrol_waitlist')."' onclick='window.history.go(-1)'></p>");
		}else{
			//$this->add_action_buttons(false, get_string('enrolme', 'enrol_waitlist'));
			global $DB;

            $canEnrol = $plugin->check_user_canenrol($instance->courseid, $USER->id);
            $isPlaces = $plugin->check_available_places($instance);
            
            if ($canEnrol){
                $lineCount = $DB->count_records('user_enrol_waitlist', array('instanceid'=>$instance->id));
                if($isPlaces){
                    $mform->addElement('html', get_string('confirmation', 'enrol_waitlist'));
                }else{
                    $mform->addElement('html', get_string('confirmationfull', 'enrol_waitlist'));
                    $mform->addElement('html', get_string('lineinfo', 'enrol_waitlist').$lineCount."<br>");
                    $mform->addElement('html', get_string('lineconfirm', 'enrol_waitlist'));
                }
                //$this->add_action_buttons(false, get_string('confirmation_yes', 'enrol_waitlist'));
                $mform->addElement("html","<br/><p align='center'><input type='submit' value='".get_string('confirmation_yes', 'enrol_waitlist')."' onclick='../../'>&nbsp;&nbsp;<input type='button' value='".get_string('confirmation_cancel', 'enrol_waitlist')."' onclick='window.history.go(-1)'></p>");   
            } else {
                $mform->addElement("html","<br/><p>To enroll into this course you need to complete courses: ".get_restricted_courses($instance->courseid)."</p>");   
            }
		}

        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);
        $mform->setDefault('id', $instance->courseid);

        $mform->addElement('hidden', 'instance');
        $mform->setType('instance', PARAM_INT);
        $mform->setDefault('instance', $instance->id);
    }

    public function validation($data, $files) {
        global $DB, $CFG;

        $errors = parent::validation($data, $files);
        $instance = $this->instance;

        if ($instance->password) {
            if ($data['enrolpassword'] !== $instance->password) {
                if ($instance->customint1) {
                    $groups = $DB->get_records('groups', array('courseid'=>$instance->courseid), 'id ASC', 'id, enrolmentkey');
                    $found = false;
                    foreach ($groups as $group) {
                        if (empty($group->enrolmentkey)) {
                            continue;
                        }
                        if ($group->enrolmentkey === $data['enrolpassword']) {
                            $found = true;
                            break;
                        }
                    }
                    if (!$found) {
                        // we can not hint because there are probably multiple passwords
                        $errors['enrolpassword'] = get_string('passwordinvalid', 'enrol_waitlist');
                    }

                } else {
                    $plugin = enrol_get_plugin('waitlist');
                    if ($plugin->get_config('showhint')) {
                        $textlib = textlib_get_instance();
                        $hint = $textlib->substr($instance->password, 0, 1);
                        $errors['enrolpassword'] = get_string('passwordinvalidhint', 'enrol_waitlist', $hint);
                    } else {
                        $errors['enrolpassword'] = get_string('passwordinvalid', 'enrol_waitlist');
                    }
                }
            }
        }

        return $errors;
    }
}


class events_handler{
    public static function user_enrolled($ue){
        global $DB;
        
        if ($ue->enrol == 'waitlist'){
            
            // send notification
            require_once('classes/notifications.php');
            $params = array(); $item = new stdClass();
            
            $course = $DB->get_record("course", array('id' => $ue->courseid));
            $user = $DB->get_record("user", array('id' => $ue->userid));
            
            $item->userid = $user->id;
            $item->student = fullname($user);
            $item->email = $user->email;
            $item->coursename = $course->fullname;
            $item->courselink = '<a href="'.$CFG->wwwroot.'/course/view.php?id='.$course->id.'">'.$course->fullname.'</a>';
            $item->coursestartdate = date('m/d/Y', $course->startdate);
            $item->courseid = $course->id;
            $params['item'] = $item;
            
            $msg = new wlNotifications(1, $params);
            $msg->process();
            
            $combo = $DB->get_record('course_combo_relations', array('courseid'=>$ue->courseid));
            if (isset($combo->combohash)){
                $combo_courses = $DB->get_records_sql("SELECT * FROM {course_combo_relations} WHERE combohash = '".$combo->combohash."' AND courseid != ".$ue->courseid);
                if (count($combo_courses)){
                    $plugin = enrol_get_plugin('waitlist');
                    foreach ($combo_courses as $course){
                        $sql = "SELECT e.*, ue.userid
                                  FROM {user_enrolments} ue
                                  JOIN {enrol} e ON (e.id = ue.enrolid AND e.enrol = 'waitlist')
                                 WHERE ue.userid = $ue->userid AND e.courseid = $course->courseid";
                        $rs = $DB->get_record_sql($sql);
                        if (!isset($rs->id)){
                            $instance = $DB->get_record_sql("SELECT e.* FROM {enrol} e WHERE e.enrol = 'waitlist' AND e.courseid = $course->courseid");
                            if ($instance){
                                $timestart = time();
                                if ($instance->enrolperiod) {
                                    $timeend = $timestart + $instance->enrolperiod;
                                } else {
                                    $timeend = 0;
                                }
                                $plugin->enrol_user($instance, $ue->userid, $instance->roleid, $timestart, $timeend);
                                $DB->delete_records('user_enrol_waitlist',array('instanceid'=>$instance->id, 'userid'=>$ue->userid));
                            }
                        }
                    }
                }
            }
        }
       
        return true;
    }
    
    public static function user_unenrolled($ue){
        global $DB, $CFG;
        
        if ($ue->enrol == 'waitlist'){
            
            // send notification
            require_once('classes/notifications.php');
            $params = array(); $item = new stdClass();
            
            $course = $DB->get_record("course", array('id' => $ue->courseid));
            $user = $DB->get_record("user", array('id' => $ue->userid));
            
            $item->userid = $user->id;
            $item->student = fullname($user);
            $item->email = $user->email;
            $item->coursename = $course->fullname;
            $item->courselink = '<a href="'.$CFG->wwwroot.'/course/view.php?id='.$course->id.'">'.$course->fullname.'</a>';
            $item->coursestartdate = date('m/d/Y', $course->startdate);
            $item->courseid = $course->id;
            $params['item'] = $item;
            
            $msg = new wlNotifications(2, $params);
            $msg->process();
            
            // combo courses
            $combo = $DB->get_record('course_combo_relations', array('courseid'=>$ue->courseid));
            if (isset($combo->combohash)){
                $combo_courses = $DB->get_records_sql("SELECT * FROM {course_combo_relations} WHERE combohash = '".$combo->combohash."' AND courseid != ".$ue->courseid);
                if (count($combo_courses)){
                    $plugin = enrol_get_plugin('waitlist');
                    foreach ($combo_courses as $course){
                        $sql = "SELECT e.*, ue.userid
                                  FROM {user_enrolments} ue
                                  JOIN {enrol} e ON (e.id = ue.enrolid AND e.enrol = 'waitlist')
                                 WHERE ue.userid = $ue->userid AND e.courseid = $course->courseid";
                        $rs = $DB->get_records_sql($sql);
                        if (count($rs)){
                            foreach ($rs as $instance){
                                unset($instance->userid);
                                $plugin->unenrol_user($instance, $ue->userid);
                            }
                        }
                    }
                }
            }
        }
        
        return true;
    } 
    
}