<?php
/**
 * *************************************************************************
 * *                  Waitlist Enrol                                      **
 * *************************************************************************
 * @copyright   emeneo.com                                                **
 * @link        emeneo.com                                                **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************
*/
class enrol_waitlist_plugin extends enrol_plugin {

    /**
     * Returns optional enrolment information icons.
     *
     * This is used in course list for quick overview of enrolment options.
     *
     * We are not using single instance parameter because sometimes
     * we might want to prevent icon repetition when multiple instances
     * of one type exist. One instance may also produce several icons.
     *
     * @param array $instances all enrol instances of this type in one course
     * @return array of pix_icon
     */
    public function get_info_icons(array $instances) {
        $key = false;
        $nokey = false;
        foreach ($instances as $instance) {
            if ($instance->password or $instance->customint1) {
                $key = true;
            } else {
                $nokey = true;
            }
        }
        $icons = array();
        if ($nokey) {
            $icons[] = new pix_icon('withoutkey', get_string('pluginname', 'enrol_waitlist'), 'enrol_waitlist');
        }
        if ($key) {
            $icons[] = new pix_icon('withkey', get_string('pluginname', 'enrol_waitlist'), 'enrol_waitlist');
        }
        return $icons;
    }

    /**
     * Returns localised name of enrol instance
     *
     * @param object $instance (null is accepted too)
     * @return string
     */
    public function get_instance_name($instance) {
        global $DB;

        if (empty($instance->name)) {
            if (!empty($instance->roleid) and $role = $DB->get_record('role', array('id'=>$instance->roleid))) {
               // $role = ' (' . role_get_name($role, get_context_instance(CONTEXT_COURSE, $instance->courseid)) . ')';
				$role = ' (' . role_get_name($role, context_course::instance($instance->courseid)) . ')';
            } else {
                $role = '';
            }
            $enrol = $this->get_name();
            return get_string('pluginname', 'enrol_'.$enrol) . $role;
        } else {
            return format_string($instance->name);
        }
    }

    public function roles_protected() {
        // users may tweak the roles later
        return false;
    }

    public function allow_unenrol(stdClass $instance) {
        // users with unenrol cap may unenrol other users manually manually
        return true;
    }

    public function allow_manage(stdClass $instance) {
        // users with manage cap may tweak period and status
        return true;
    }

    public function show_enrolme_link(stdClass $instance) {
        return ($instance->status == ENROL_INSTANCE_ENABLED);
    }

    /**
     * Sets up navigation entries.
     *
     * @param object $instance
     * @return void
     */
	 /*
    public function add_course_navigation($instancesnode, stdClass $instance) {
        if ($instance->enrol !== 'waitlist') {
             throw new coding_exception('Invalid enrol instance type!');
        }

        $context = get_context_instance(CONTEXT_COURSE, $instance->courseid);
        if (has_capability('enrol/waitlist:config', $context)) {
            $managelink = new moodle_url('/enrol/waitlist/edit.php', array('courseid'=>$instance->courseid, 'id'=>$instance->id));
            $instancesnode->add($this->get_instance_name($instance), $managelink, navigation_node::TYPE_SETTING);
        }
    }
	*/
	
	/**
     * Sets up navigation entries.
     *
     * @param stdClass $instancesnode
     * @param stdClass $instance
     * @return void
     */
    public function add_course_navigation($instancesnode, stdClass $instance) {
        if ($instance->enrol !== 'waitlist') {
             throw new coding_exception('Invalid enrol instance type!');
        }

        $context = context_course::instance($instance->courseid);
        if (has_capability('enrol/waitlist:config', $context)) {
            $managelink = new moodle_url('/enrol/waitlist/edit.php', array('courseid'=>$instance->courseid, 'id'=>$instance->id));
            $instancesnode->add($this->get_instance_name($instance), $managelink, navigation_node::TYPE_SETTING);
        }
    }
    
    
    /**
     * Enrol user into course via enrol instance.
     *
     * @param stdClass $instance
     * @param int $userid
     * @param int $roleid optional role id
     * @param int $timestart 0 means unknown
     * @param int $timeend 0 means forever
     * @param int $status default to ENROL_USER_ACTIVE for new enrolments, no change by default in updates
     * @param bool $recovergrades restore grade history
     * @return void
     */
    public function enrol_user(stdClass $instance, $userid, $roleid = null, $timestart = 0, $timeend = 0, $status = null, $recovergrades = null) {
        parent::enrol_user($instance, $userid, null, $timestart, $timeend, $status, $recovergrades);
        if ($roleid) {
            $context = context_course::instance($instance->courseid, MUST_EXIST);
            role_assign($roleid, $userid, $context->id, 'enrol_'.$this->get_name(), $instance->id);
        }
    }
    
    public function check_available_places(stdClass $instance, $with_combo = true) {
        global $DB;
        $canEnrol = false;
        $enroledCount = $DB->get_record_sql("SELECT COUNT(ue.id) as users 
                                                FROM {user_enrolments} ue
                                                    LEFT JOIN {enrol} e ON e.id = ue.enrolid
                                                    LEFT JOIN {course_completions} cc ON cc.course = e.courseid AND cc.userid = ue.userid
                                                WHERE ue.enrolid = $instance->id AND (cc.timecompleted = 0 OR cc.timecompleted IS NULL)");
        
        if($instance->customint3 == 0){
            $canEnrol = true;
        }elseif($enroledCount->users < $instance->customint3){
            $canEnrol = true;
            if($instance->enrolenddate){
                if(time() > $instance->enrolenddate){
                    $canEnrol = false;
                }
            }
        }
        if ($canEnrol and $with_combo){
            $combo = $DB->get_record('course_combo_relations', array('courseid'=>$instance->courseid));
            if (isset($combo->combohash)){
                $combo_courses = $DB->get_records_sql("SELECT * FROM {course_combo_relations} WHERE combohash = '".$combo->combohash."' AND courseid != ".$instance->courseid);
                if (count($combo_courses)){
                    foreach ($combo_courses as $course){
                        $instance = $DB->get_record_sql("SELECT e.* FROM {enrol} e WHERE e.enrol = 'waitlist' AND e.courseid = $course->courseid");
                        if ($instance){
                           $canEnrol = $this->check_available_places($instance, false);
                           if (!$canEnrol) break;
                        }
                    }
                }
            }
        }
        
        return $canEnrol;
    }
    
    public function check_user_canenrol($courseid = 0, $userid = 0) {
        global $DB, $USER;
        $canEnrol = false;
        $userid = ($userid > 0) ? $userid : $USER->id;
        
        $restrictCourses = $DB->get_records_sql("SELECT ra.*, c.fullname, cc.timecompleted 
                                                    FROM {course_restrict_access} ra
                                                        LEFT JOIN {course} c ON c.id = ra.coursechild
                                                        LEFT JOIN {course_completions} cc ON cc.course = c.id AND cc.userid = $userid
                                                    WHERE ra.courseparent = $courseid AND c.id > 1 AND (cc.timecompleted = 0 OR cc.timecompleted IS NULL)");
        if (count($restrictCourses) == 0){
            $canEnrol = true;
        }
        
        return $canEnrol;
    }
   
	/*
	public function unenrol_user($enrolId){
		global $DB;
		global $CFG;

		$enrolment = $DB->get_record('user_enrolments', array('id'=>$enrolId), '*', MUST_EXIST);
		$instance = $DB->get_record('enrol', array('id'=>$enrolment->enrolid), '*', MUST_EXIST);

		require_once("$CFG->dirroot/enrol/waitlist/waitlist.php");
		$waitlist = new waitlist();
		$res = $waitlist->add_wait_list($instance->id, $enrolment->userid, $instance->roleid, $enrolment->timestart, $enrolment->timeend);
		if($res){
			$DB->delete_records('user_enrolments',array('id'=>$enrolId));
		}
	}
	*/

    /**
     * Returns edit icons for the page with list of instances
     * @param stdClass $instance
     * @return array
     */
    public function get_action_icons(stdClass $instance) {
        global $OUTPUT;

        if ($instance->enrol !== 'waitlist') {
            throw new coding_exception('invalid enrol instance!');
        }
        //$context = get_context_instance(CONTEXT_COURSE, $instance->courseid);
		$context = context_course::instance($instance->courseid);

        $icons = array();

		if (has_capability('enrol/waitlist:config', $context)) {
            $managelink = new moodle_url("/enrol/waitlist/enroluser.php", array('enrolid'=>$instance->id));
			$icons[] = $OUTPUT->action_icon($managelink, new pix_icon('t/enrolusers', get_string('enrolusers', 'enrol_waitlist'), 'core', array('class'=>'iconsmall')));
        }

        if (has_capability('enrol/waitlist:config', $context)) {
            $editlink = new moodle_url("/enrol/waitlist/edit.php", array('courseid'=>$instance->courseid, 'id'=>$instance->id));
            $icons[] = $OUTPUT->action_icon($editlink, new pix_icon('t/edit', get_string('edit'), 'core', array('class'=>'iconsmall')));
        }

		if (has_capability('enrol/waitlist:config', $context)) {
            $editlink = new moodle_url("/enrol/waitlist/users.php", array('id'=>$instance->courseid));
            $icons[] = $OUTPUT->action_icon($editlink, new pix_icon('i/switchrole', get_string('waitlisted_users','enrol_waitlist'), 'core', array('class'=>'iconsmall')));
        }
        
        if (has_capability('enrol/waitlist:config', $context)) {
            $editlink = new moodle_url("/enrol/waitlist/restrict.php", array('id'=>$instance->courseid));
            $icons[] = $OUTPUT->action_icon($editlink, new pix_icon('i/permissions', get_string('enrollment_dependencies','enrol_waitlist'), 'core', array('class'=>'iconsmall')));
        }
        
        if (has_capability('enrol/waitlist:config', $context)) {
            $editlink = new moodle_url("/enrol/waitlist/combo.php", array('id'=>$instance->courseid));
            $icons[] = $OUTPUT->action_icon($editlink, new pix_icon('i/db', get_string('combo_courses','enrol_waitlist'), 'core', array('class'=>'iconsmall')));
        }

        return $icons;
    }

    /**
     * Returns link to page which may be used to add new instance of enrolment plugin in course.
     * @param int $courseid
     * @return moodle_url page url
     */
    public function get_newinstance_link($courseid) {
        //$context = get_context_instance(CONTEXT_COURSE, $courseid, MUST_EXIST);
		$context = context_course::instance($courseid);

        if (!has_capability('moodle/course:enrolconfig', $context) or !has_capability('enrol/waitlist:config', $context)) {
            return NULL;
        }
        // multiple instances supported - different roles with different password
        return new moodle_url('/enrol/waitlist/edit.php', array('courseid'=>$courseid));
    }

    /**
     * Creates course enrol form, checks if form submitted
     * and enrols user if necessary. It can also redirect.
     *
     * @param stdClass $instance
     * @return string html text, usually a form in a text box
     */
    public function enrol_page_hook(stdClass $instance) {
        global $CFG, $OUTPUT, $SESSION, $USER, $DB;

        if (isguestuser()) {
            // can not enrol guest!!
            return null;
        }
        if ($DB->record_exists('user_enrolments', array('userid'=>$USER->id, 'enrolid'=>$instance->id))) {
            //TODO: maybe we should tell them they are already enrolled, but can not access the course
            return null;
			
        }

		if($DB->record_exists('user_enrol_waitlist', array('userid'=>$USER->id, 'instanceid'=>$instance->id))){
			return $OUTPUT->notification(get_string('waitlistinfo', 'enrol_waitlist'));
		}

        if ($instance->enrolstartdate != 0 and $instance->enrolstartdate > time()) {
            //TODO: inform that we can not enrol yet
            return null;
        }
		/*
        if ($instance->enrolenddate != 0 and $instance->enrolenddate < time()) {
            //TODO: inform that enrolment is not possible any more
            return null;
        }
		
        if ($instance->customint3 > 0) {
            // max enrol limit specified
            $count = $DB->count_records('user_enrolments', array('enrolid'=>$instance->id));
            if ($count >= $instance->customint3) {
                // bad luck, no more waitlist enrolments here
                return $OUTPUT->notification(get_string('maxenrolledreached', 'enrol_waitlist'));
            }
        }
		*/
        require_once("$CFG->dirroot/enrol/waitlist/locallib.php");
        require_once("$CFG->dirroot/group/lib.php");
		require_once("$CFG->dirroot/enrol/waitlist/waitlist.php");

		$waitlist = new waitlist();
		
		/*
		if(!$waitlist->vaildate_wait_list($instance->id,$USER->id)){
			return $OUTPUT->notification(get_string('waitlistinfo', 'enrol_waitlist'));
		}
		*/

        $form = new enrol_waitlist_enrol_form(NULL, $instance);
        $instanceid = optional_param('instance', 0, PARAM_INT);

        if ($instance->id == $instanceid) {
            if ($data = $form->get_data()) {
                $enrol = enrol_get_plugin('waitlist');
                $timestart = time();
                if ($instance->enrolperiod) {
                    $timeend = $timestart + $instance->enrolperiod;
                } else {
                    $timeend = 0;
                }
                
                $isPlaces = $enrol->check_available_places($instance);
                $canEnrol = $enrol->check_user_canenrol($instance->courseid, $USER->id);

                if($canEnrol){
                    if($isPlaces){
                        $this->enrol_user($instance, $USER->id, $instance->roleid, $timestart, $timeend);
                    }else{
                        $waitlist->add_wait_list($instance->id, $USER->id, $instance->roleid, $timestart, $timeend);
                    }
                    add_to_log($instance->courseid, 'course', 'enrol', '../enrol/users.php?id='.$instance->courseid, $instance->courseid); //there should be userid somewhere!
                    
                    if ($instance->password and $instance->customint1 and $data->enrolpassword !== $instance->password) {
                        // it must be a group enrolment, let's assign group too
                        $groups = $DB->get_records('groups', array('courseid'=>$instance->courseid), 'id', 'id, enrolmentkey');
                        foreach ($groups as $group) {
                            if (empty($group->enrolmentkey)) {
                                continue;
                            }
                            if ($group->enrolmentkey === $data->enrolpassword) {
                                groups_add_member($group->id, $USER->id);
                                break;
                            }
                        }
                    }
                }

				redirect("$CFG->wwwroot/course/view.php?id=$instance->courseid");
            }
        }

        ob_start();
        $form->display();
        $output = ob_get_clean();
        return $OUTPUT->box($output);
    }

    /**
     * Add new instance of enrol plugin with default settings.
     * @param object $course
     * @return int id of new instance
     */
    public function add_default_instance($course) {
        $fields = array('customint1'  => $this->get_config('groupkey'),
                        'customint2'  => $this->get_config('longtimenosee'),
                        'customint3'  => $this->get_config('maxenrolled'),
                        'customint5'  => $this->get_config('beforeunenroll'),
                        'customchar1'  => $this->get_config('faculty'),
                        'enrolperiod' => $this->get_config('enrolperiod', 0),
                        'status'      => $this->get_config('status'),
                        'roleid'      => $this->get_config('roleid', 0));

        if ($this->get_config('requirepassword')) {
            $fields['password'] = generate_password(20);
        }

        return $this->add_instance($course, $fields);
    }

    /**
     * Send welcome email to specified user
     *
     * @param object $instance
     * @param object $user user record
     * @return void
     */
    protected function email_welcome_message($instance, $user) {
        global $CFG, $DB;

        $course = $DB->get_record('course', array('id'=>$instance->courseid), '*', MUST_EXIST);

        $a = new stdClass();
        $a->coursename = format_string($course->fullname);
        $a->profileurl = "$CFG->wwwroot/user/view.php?id=$user->id&course=$course->id";
		$a->summary = $course->summary;
		$a->startdate = '';
		if($course->startdate != 0){
			$a->startdate = date('Y-m-d',$course->startdate);
		}

        if (trim($instance->customtext1) !== '') {
            $message = $instance->customtext1;
            $message = str_replace('{$a->coursename}', $a->coursename, $message);
            $message = str_replace('{$a->profileurl}', $a->profileurl, $message);
			$message = str_replace('{$a->summary}', $a->summary, $message);
			$message = str_replace('{$a->startdate}', $a->startdate, $message);
        } else {
            $message = get_string('welcometocoursetext', 'enrol_waitlist', $a);
        }

        $subject = get_string('welcometocourse', 'enrol_waitlist', format_string($course->fullname));

        //$context = get_context_instance(CONTEXT_COURSE, $course->id);
		$context = context_course::instance($course->id);
        $rusers = array();
        if (!empty($CFG->coursecontact)) {
            $croles = explode(',', $CFG->coursecontact);
            $rusers = get_role_users($croles, $context, true, '', 'r.sortorder ASC, u.lastname ASC');
        }
        if ($rusers) {
            $contact = reset($rusers);
        } else {
            $contact = get_admin();
        }

        //directly emailing welcome message rather than using messaging
        email_to_user($user, $contact, $subject, '',$message);
    }

    /**
     * Enrol waitlist cron support
     * @return void
     */
    public function cron() {
        global $DB, $CFG;
        require_once('classes/notifications.php');

        if (!enrol_is_enabled('waitlist')) {
            return;
        }

        $plugin = enrol_get_plugin('waitlist');

        $now = time();

        //note: the logic of waitlist enrolment guarantees that user logged in at least once (=== u.lastaccess set)
        //      and that user accessed course at least once too (=== user_lastaccess record exists)
        
        $sql = "SELECT e.* FROM {enrol} e WHERE e.enrol = 'waitlist' AND e.customint5 > 0";
        $rs = $DB->get_recordset_sql($sql);
        if (count($rs)){
            foreach ($rs as $instance){
                $sql = "SELECT u.id, u.email, CONCAT(u.firstname, ' ', u.lastname) as username, c.fullname, c.id as courseid, c.startdate, u.lastaccess, e.customint5
                  FROM {user_enrolments} ue
                  LEFT JOIN {enrol} e ON e.id = ue.enrolid
                  LEFT JOIN {course} c ON c.id = e.courseid
                  LEFT JOIN {user} u ON u.id = ue.userid
                  LEFT JOIN {waitlist_notifications_logs} nl ON nl.userid = ue.userid AND nl.notid = 3 AND nl.dataid = e.courseid AND nl.datatype = 'course' 
                 WHERE (($now - u.lastaccess) > (e.customint2 - e.customint5)) AND (($now - u.lastaccess) <= (e.customint2 - (e.customint5 + 86399))) AND u.deleted = 0 AND nl.id IS NULL AND e.id = $instance->id";
                $users = $DB->get_records_sql($sql);
                foreach ($users as $user) {
                    $params = array(); $item = new stdClass();
                    $item->userid = $user->id;
                    $item->student = $user->username;
                    $item->email = $user->email;
                    $item->coursename = $user->fullname;
                    $item->courselink = '<a href="'.$CFG->wwwroot.'/course/view.php?id='.$user->courseid.'">'.$user->fullname.'</a>';
                    $item->coursestartdate = date('m/d/Y', $user->startdate);
                    $item->unenrolldate = date('m/d/Y', ($now+$user->customint5));
                    $item->courseid = $user->courseid;
                    $params['item'] = $item;

                    $msg = new wlNotifications(3, $params);
                    $msg->process();
                }
            }
        }
        $rs->close();
        
        $sql = "SELECT e.* FROM {enrol} e WHERE e.enrol = 'waitlist' AND e.customint5 > 0";
        $rs = $DB->get_recordset_sql($sql);
        if (count($rs)){
            foreach ($rs as $instance){
                $sql = "SELECT u.id, u.email, CONCAT(u.firstname, ' ', u.lastname) as username, c.fullname, c.id as courseid, c.startdate, ul.timeaccess, e.customint5
                  FROM {user_enrolments} ue
                  LEFT JOIN {enrol} e ON e.id = ue.enrolid
                  LEFT JOIN {course} c ON c.id = e.courseid
                  LEFT JOIN {user} u ON u.id = ue.userid
                  LEFT JOIN {user_lastaccess} ul ON (ul.userid = ue.userid AND ul.courseid = e.courseid)
                  LEFT JOIN {waitlist_notifications_logs} nl ON nl.userid = ue.userid AND nl.notid = 3 AND nl.dataid = e.courseid AND nl.datatype = 'course' 
                 WHERE (($now - ul.timeaccess) > (e.customint2 - e.customint5)) AND (($now - ul.timeaccess) <= (e.customint2 - (e.customint5 + 86399))) AND u.deleted = 0 AND nl.id IS NULL AND e.id = $instance->id";
                $users = $DB->get_records_sql($sql);
                foreach ($users as $user) {
                    $params = array(); $item = new stdClass();
                    $item->userid = $user->id;
                    $item->student = $user->username;
                    $item->email = $user->email;
                    $item->coursename = $user->fullname;
                    $item->courselink = '<a href="'.$CFG->wwwroot.'/course/view.php?id='.$user->courseid.'">'.$user->fullname.'</a>';
                    $item->coursestartdate = date('m/d/Y', $user->startdate);
                    $item->unenrolldate = date('m/d/Y', ($now+$user->customint5));
                    $item->courseid = $user->courseid;
                    $params['item'] = $item;

                    $msg = new wlNotifications(3, $params);
                    $msg->process();
                }
            }
        }
        $rs->close();

        // first deal with users that did not log in for a really long time
        $sql = "SELECT e.*, ue.userid
                  FROM {user_enrolments} ue
                  JOIN {enrol} e ON (e.id = ue.enrolid AND e.enrol = 'waitlist' AND e.customint2 > 0)
                  JOIN {user} u ON u.id = ue.userid
                 WHERE :now - u.lastaccess > e.customint2";
        $rs = $DB->get_recordset_sql($sql, array('now'=>$now));
        foreach ($rs as $instance) {
            $userid = $instance->userid;
            unset($instance->userid);
            $plugin->unenrol_user($instance, $userid);
            mtrace("unenrolling user $userid from course $instance->courseid as they have did not log in for $instance->customint2 days");
        }
        $rs->close();

        // now unenrol from course user did not visit for a long time
        $sql = "SELECT e.*, ue.userid
                  FROM {user_enrolments} ue
                  JOIN {enrol} e ON (e.id = ue.enrolid AND e.enrol = 'waitlist' AND e.customint2 > 0)
                  JOIN {user_lastaccess} ul ON (ul.userid = ue.userid AND ul.courseid = e.courseid)
                 WHERE :now - ul.timeaccess > e.customint2";
        $rs = $DB->get_recordset_sql($sql, array('now'=>$now));
        foreach ($rs as $instance) {
            $userid = $instance->userid;
            unset($instance->userid);
            $plugin->unenrol_user($instance, $userid);
            mtrace("unenrolling user $userid from course $instance->courseid as they have did not access course for $instance->customint2 days");
        }
        $rs->close();

		//wait list
		$this->process_restrict_access();
		$this->process_wait_list();
        
        $waitlist_notifications = $DB->get_records_sql("SELECT id FROM {waitlist_notifications_logs} WHERE timesend < ($now - 2592000)");
        foreach ($waitlist_notifications as $notification){
            $DB->delete_records('waitlist_notifications_logs', array('id'=>$notification->id));   
        }

        flush();
    }

	public function get_user_enrolment_actions(course_enrolment_manager $manager, $ue) {
        $actions = array();
        $context = $manager->get_context();
        $instance = $ue->enrolmentinstance;
        $params = $manager->get_moodlepage()->url->params();
        $params['ue'] = $ue->id;
        if ($this->allow_unenrol($instance) && has_capability("enrol/self:unenrol", $context)) {
            $url = new moodle_url('/enrol/unenroluser.php', $params);
            $actions[] = new user_enrolment_action(new pix_icon('t/delete', ''), get_string('unenrol', 'enrol'), $url, array('class'=>'unenrollink', 'rel'=>$ue->id));
        }
        if ($this->allow_manage($instance) && has_capability("enrol/self:manage", $context)) {
            $url = new moodle_url('/enrol/self/editenrolment.php', $params);
            $actions[] = new user_enrolment_action(new pix_icon('t/edit', ''), get_string('edit'), $url, array('class'=>'editenrollink', 'rel'=>$ue->id));
        }
        return $actions;
    }

/**
 * Is it possible to hide/show enrol instance via standard UI?
 *
 * @param stdClass $instance
 * @return bool
 */
public function can_hide_show_instance($instance) {
    $context = context_course::instance($instance->courseid);
    return has_capability('enrol/waitlist:config', $context);
}

/**
 * Is it possible to delete enrol instance via standard UI?
 *
 * @param stdClass $instance
 * @return bool
 */
public function can_delete_instance($instance) {
    $context = context_course::instance($instance->courseid);
    return has_capability('enrol/waitlist:config', $context);
}
	
	//cron process waitlist - 2012-11-01
	public function process_wait_list(){
		global $DB, $CFG;
		require_once("$CFG->dirroot/enrol/waitlist/waitlist.php");
		$waitlist = new waitlist();

		$rows = $waitlist->get_wait_list();
		foreach($rows as $row){
			$userCount = $DB->count_records('user', array('id'=>$row->userid,'deleted'=>0));
			if(!$userCount){
				$DB->delete_records('user_enrol_waitlist',array('id'=>$row->id));
				continue;
			}

			$instanceCount = $DB->count_records('enrol', array('id'=>$row->instanceid));
			if(!$instanceCount){
				$DB->delete_records('user_enrol_waitlist',array('id'=>$row->id));
				continue;
			}
			$instance = $DB->get_record_sql("select * from ".$CFG->prefix."enrol where id=".$row->instanceid);

			if($instance){
				if(!$instance->id){
					continue;
				}
                if ($instance->customint2 > 0){
                    $userdata = $DB->get_record("user", array('id' => $row->userid));
                    if ((time()-$userdata->lastaccess) > $instance->customint2){
                        continue;
                    }
                }
                
                $isPlaces = $this->check_available_places($instance);
				if($isPlaces){
					if($instance->enrolenddate){
						if(time()>$instance->enrolenddate){
							continue;
						}
					}
					$this->enrol_user($instance, $row->userid, $row->roleid, $row->timestart, $row->timeend);
					$DB->delete_records('user_enrol_waitlist',array('id'=>$row->id));
				}
			}
		}
	}
    
    public function process_restrict_access(){
        global $DB, $CFG;
        
        $restricted_courses = array();
        $child_courses = array();
        $completed_users = array();
        $all_users = array();
        $courses = $DB->get_records_sql("SELECT ra.* FROM {course_restrict_access} ra
                                            LEFT JOIN {course} c ON c.id = ra.courseparent
                                        WHERE c.id > 1 ORDER BY c.id");
        if (count($courses) > 0){
            foreach ($courses as $course){
                $restricted_courses[$course->courseparent][$course->coursechild] = $course->coursechild;
                $child_courses[$course->coursechild] = $course->coursechild;
            }
        }
        
        if (count($child_courses) > 0){
            foreach ($child_courses as $courseitem){
                $completions = $DB->get_records_sql("SELECT cc.id, cc.course, cc.userid 
                                                            FROM {course_completions} cc
                                                                LEFT JOIN {course} c ON c.id = cc.course
                                                                LEFT JOIN {user} u ON u.id = cc.userid
                                                            WHERE cc.course = $courseitem AND cc.timecompleted > 0 AND u.id > 0 AND c.id > 1");
                if (count($completions) > 0){
                    foreach ($completions as $completion){
                        $completed_users[$courseitem][$completion->userid] = $completion->userid;
                        $all_users[$completion->userid] = $completion->userid;
                    }
                } else {
                    $completed_users[$courseitem] = array();
                }
            }
        }
        
        $cusers = array();
        if (count($restricted_courses) > 0 and count($completed_users) > 0){
            foreach ($restricted_courses as $courseparent=>$coursechild){
                $restricted_completed[$courseparent] = array(1=>$all_users);
                foreach ($coursechild as $course){
                    $restricted_completed[$courseparent][$course] = $completed_users[$course];
                }
                
                $cusers[$courseparent] = call_user_func_array('array_intersect', $restricted_completed[$courseparent]);
            }
        }
        
        if (count($cusers) > 0){
            require_once("$CFG->dirroot/enrol/waitlist/waitlist.php");
            $waitlist = new waitlist();
            
            foreach($cusers as $courseid=>$users){
                if (count($users) > 0){
                    $instance = $DB->get_record_sql("SELECT e.* FROM {enrol} e WHERE e.enrol = 'waitlist' AND e.courseid = $courseid");
                    
                    if ($instance){
                        $timestart = time();
                        if ($instance->enrolperiod) {
                            $timeend = $timestart + $instance->enrolperiod;
                        } else {
                            $timeend = 0;
                        }
                        if($instance->enrolenddate){
                            if(time()>$instance->enrolenddate){
                                continue;
                            }
                        }
                        
                        foreach ($users as $user){
                            if ($instance->customint2 > 0){
                                $userdata = $DB->get_record("user", array('id' => $user));
                                if ((time()-$userdata->lastaccess) > $instance->customint2){
                                    continue;
                                }
                            }
                            
                            $isPlaces = $this->check_available_places($instance);
                            if($isPlaces){
                                $is_record = $DB->get_record('user_enrolments', array('enrolid'=>$instance->id, 'userid'=>$user));
                                
                                if (!$is_record){
                                    $this->enrol_user($instance, $user, $instance->roleid, $timestart, $timeend);
                                    $DB->delete_records('user_enrol_waitlist',array('userid'=>$user, 'instanceid'=>$instance->id));
                                }
                            } else {
                                $is_enrol = $DB->get_record('user_enrolments', array('enrolid'=>$instance->id, 'userid'=>$user));
                                $is_record = $DB->get_record('user_enrol_waitlist', array('instanceid'=>$instance->id, 'userid'=>$user));
                                if (!$is_enrol and !$is_record){
                                    $waitlist->add_wait_list($instance->id, $user, $instance->roleid, $timestart, $timeend);
                                }
                            }
                        }
                    }
                }
            }
        }
        
    }
}

/**
 * Indicates API features that the enrol plugin supports.
 *
 * @param string $feature
 * @return mixed True if yes (some features may use other values)
 */
function enrol_waitlist_supports($feature) {
    switch($feature) {
        case ENROL_RESTORE_TYPE: return ENROL_RESTORE_EXACT;

        default: return null;
    }
}

function get_restricted_courses($courseid){
    global $DB;
    $output = ''; $courses = array();
    
    $restrictCourses = $DB->get_records_sql("SELECT ra.*, c.fullname
                                                    FROM {course_restrict_access} ra
                                                        LEFT JOIN {course} c ON c.id = ra.coursechild
                                                    WHERE ra.courseparent = $courseid AND c.id > 1 ORDER BY c.fullname");
    if (count($restrictCourses) > 0){
        foreach ($restrictCourses as $course){
            $courses[] = '<a href="'.$CFG->wwwroot.'/course/view.php?id='.$course->coursechild.'">'.$course->fullname.'</a>';
        }
        $output = implode(', ', $courses);
    }
    
    return $output;
}

