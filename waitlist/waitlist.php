<?php
/**
 * *************************************************************************
 * *                  Waitlist Enrol                                      **
 * *************************************************************************
 * @copyright   emeneo.com                                                **
 * @link        emeneo.com                                                **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************
*/
class waitlist{
	public function add_wait_list($instanceid, $userid, $roleid, $timestart, $timeend, $with_combo = true){
		global $DB, $CFG;

        $is_record = $DB->get_record('user_enrol_waitlist', array('instanceid'=>$instanceid, 'userid'=>$userid));
		
        $waitlist = new stdClass();
		$waitlist->userid = $userid;
		$waitlist->instanceid = $instanceid;
		$waitlist->roleid = $roleid;
		$waitlist->timestart = $timestart;
		$waitlist->timeend = $timeend;
		$waitlist->timecreated = time();
        
        if ($is_record){
            $waitlist->id = $is_record->id;
            $DB->update_record('user_enrol_waitlist', $waitlist);   
        } else {
            $id = $DB->insert_record('user_enrol_waitlist', $waitlist);
            
            require_once('classes/notifications.php');
            $params = array(); $item = new stdClass();
            
            $instance = $DB->get_record("enrol", array('id' => $instanceid));
            $course = $DB->get_record("course", array('id' => $instance->courseid));
            $user = $DB->get_record("user", array('id' => $userid));
            
            $item->userid = $user->id;
            $item->student = fullname($user);
            $item->email = $user->email;
            $item->coursename = $course->fullname;
            $item->courselink = '<a href="'.$CFG->wwwroot.'/course/view.php?id='.$course->id.'">'.$course->fullname.'</a>';
            $item->coursestartdate = date('m/d/Y', $course->startdate);
            $item->courseid = $course->id;
            $params['item'] = $item;
            
            $msg = new wlNotifications(4, $params);
            $msg->process();
        }
        
        if ($with_combo){
            $this->add_wait_list_combo($instanceid, $userid, $roleid, $timestart, $timeend);
        }
        
        return $id;
	}
    
    public function add_wait_list_combo($instanceid, $userid, $roleid, $timestart, $timeend){
        global $DB;
        
        $instance = $DB->get_record("enrol", array('id' => $instanceid));
        $combo = $DB->get_record('course_combo_relations', array('courseid'=>$instance->courseid));
        
        if (isset($combo->combohash)){
            $combo_courses = $DB->get_records_sql("SELECT * FROM {course_combo_relations} WHERE combohash = '".$combo->combohash."' AND courseid != ".$instance->courseid);
            
            if (count($combo_courses)){
                foreach ($combo_courses as $course){
                    $sql = "SELECT e.*, ue.userid
                              FROM {user_enrolments} ue
                              JOIN {enrol} e ON (e.id = ue.enrolid AND e.enrol = 'waitlist')
                             WHERE ue.userid = $userid AND e.courseid = $course->courseid";
                    $rs = $DB->get_record_sql($sql);
                    if (!isset($rs->id)){
                        $instance_child = $DB->get_record_sql("SELECT e.* FROM {enrol} e WHERE e.enrol = 'waitlist' AND e.courseid = $course->courseid");
                        
                        $re = $DB->get_record('user_enrol_waitlist', array('instanceid'=>$instance_child->id, 'userid'=>$userid));
                        if (!$re){
                            $this->add_wait_list($instance_child->id, $userid, $roleid, $timestart, $timeend, false);   
                        }
                    }
                }
            }
        }
        
        return true;
    }

	public function vaildate_wait_list($instanceid, $userid){
		global $DB;
		global $CFG;

		$res = $DB->get_records_sql("select * from ".$CFG->prefix."user_enrol_waitlist where instanceid=".$instanceid." and userid=".$userid);
		if(count($res)){
			return false;
		}else{
			return true;
		}
	}

	public function get_wait_list(){
		global $DB;
		global $CFG;
		
		return $DB->get_records_sql("select * from ".$CFG->prefix."user_enrol_waitlist ORDER BY id");
	}
}