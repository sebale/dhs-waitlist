<?php
/**
 * ELIS(TM): Enterprise Learning Intelligence Suite
 * Copyright (C) 2008-2013 Remote-Learner.net Inc (http://www.remote-learner.net)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

function xmldb_enrol_waitlist_install() {
    global $CFG, $DB;

    // Notifications
    $notifications = array();
    $notifications[] = array('id'=>1, 
                             'type'=>'user', 
                             'name'=>'User enrolled', 
                             'description'=>'Sends when user was enrolled to course', 
                             'tags'=>'a:7:{i:0;s:14:"recipient name";i:1;s:15:"recipient email";i:2;s:17:"support user name";i:3;s:13:"support email";i:4;s:11:"course name";i:5;s:11:"course link";s:17:"course start date";s:17:"course start date";}', 
                             'subject'=>'Welcome to [course name]', 
                             'body'=>'Hello, [recipient name]<br><br>Welcome to [course name]<br><br>Best reguards,<br>[support user name]<br>', 
                             'sender'=>'supportuser', 
                             'status'=>1, 
                             'role'=>'Student', 
                             'category'=>'Student notifications', 
                             'sortorder	'=>'1');
    
    $notifications[] = array('id'=>2, 
                             'type'=>'user', 
                             'name'=>'User unenrolled', 
                             'description'=>'Sends when user was unenrolled from course', 
                             'tags'=>'a:6:{i:0;s:14:"recipient name";i:1;s:15:"recipient email";i:2;s:17:"support user name";i:3;s:13:"support email";i:4;s:11:"course name";i:5;s:11:"course link";} ', 
                             'subject'=>'You was unenrolled from [course name]', 
                             'body'=>"Hello, [recipient name]<br><br>You was unenrolled from [course name]<br><br>Best reguards,<br>[support user name]<br>", 
                             'sender'=>'supportuser', 
                             'sendto'=>'', 
                             'status'=>1, 
                             'role'=>'Student', 
                             'category'=>'Student notifications', 
                             'sortorder	'=>'2');
    
    $notifications[] = array('id'=>3, 
                             'type'=>'user', 
                             'name'=>'User will be auto unenrolled', 
                             'description'=>'Sending before user will be auto unenrolled', 
                             'tags'=>'a:7:{i:0;s:14:"recipient name";i:1;s:15:"recipient email";i:2;s:17:"support user name";i:3;s:13:"support email";i:4;s:11:"course name";i:5;s:11:"course link";i:6;s:13:"unenroll date";}', 
                             'subject'=>'You will be unenrolled from course [course name]', 
                             'body'=>"Hello, [recipient name]<br><br>You will be unenrolled from course [course name] on [unenroll date]<br><br>Best reguards,<br>[support user name]<br>", 
                             'sender'=>'supportuser', 
                             'sendto'=>'', 
                             'status'=>1,  
                             'role'=>'Student', 
                             'category'=>'Student notifications', 
                             'sortorder	'=>'3');
    
    $notifications[] = array('id'=>4, 
                             'type'=>'user', 
                             'name'=>'User added to waitlist', 
                             'description'=>'Sends when user was added to waitlist', 
                             'tags'=>'a:7:{i:0;s:14:"recipient name";i:1;s:15:"recipient email";i:2;s:17:"support user name";i:3;s:13:"support email";i:4;s:11:"course name";i:5;s:11:"course link";s:17:"course start date";s:17:"course start date";}', 
                             'subject'=>'You was added to waitlist on course [course name]', 
                             'body'=>"Hello, [recipient name]<br><br>You was added to waitlist on course [course name]<br><br>Best reguards,<br>[support user name]<br>", 
                             'sender'=>'supportuser', 
                             'status'=>1,  
                             'role'=>'Student', 
                             'category'=>'Student notifications', 
                             'sortorder	'=>'4');
    
    foreach ($notifications as $notification){
        $new_notification = $notification;
        $record = $DB->get_record('waitlist_notifications', array('id'=>$notification['id']));
        if (!$record){
            unset($new_notification->id);
            $DB->insert_record('waitlist_notifications', $new_notification, false);   
        }
    }
    
}


