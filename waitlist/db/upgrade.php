<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

function xmldb_enrol_waitlist_upgrade($oldversion) {
    global $CFG, $DB;

    $dbman = $DB->get_manager();

    
    /* user_enrol_waitlist */
    
	// Define table user_enrol_waitlist to be created.
	$table = new xmldb_table('user_enrol_waitlist');

	// Adding fields to table user_enrol_waitlist.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('instanceid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('roleid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('timestart', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('timeend', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    
	// Adding keys to table user_enrol_waitlist.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for user_enrol_waitlist.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    
    /* course_combo_relations */
    
    // Define table course_combo_relations to be created.
	$table = new xmldb_table('course_combo_relations');

	// Adding fields to table course_combo_relations.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
	$table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
	$table->add_field('combohash', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    
	// Adding keys to table course_combo_relations.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for course_combo_relations.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    
    /* course_restrict_access */
    
    // Define table course_restrict_access to be created.
	$table = new xmldb_table('course_restrict_access');

	// Adding fields to table course_restrict_access.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('courseparent', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('coursechild', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    
	// Adding keys to table course_restrict_access.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for course_restrict_access.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    
    /* waitlist_notifications */
    
    // Define table waitlist_notifications to be created.
	$table = new xmldb_table('waitlist_notifications');

	// Adding fields to table waitlist_notifications.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('type', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('description', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('tags', XMLDB_TYPE_TEXT, null, null, null, null, null);
    $table->add_field('subject', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('body', XMLDB_TYPE_TEXT, null, null, null, null, null);
    $table->add_field('sender', XMLDB_TYPE_CHAR, '255', null, null, null, 'supportuser');
    $table->add_field('sendto', XMLDB_TYPE_TEXT, null, null, null, null, null);
    $table->add_field('status', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '1');
    $table->add_field('role', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('category', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('sortorder', XMLDB_TYPE_INTEGER, '5', null, XMLDB_NOTNULL, null, '0');
    
	// Adding keys to table waitlist_notifications.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for waitlist_notifications.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    
    /* waitlist_notifications_logs */
    
    // Define table waitlist_notifications_logs to be created.
	$table = new xmldb_table('waitlist_notifications_logs');

	// Adding fields to table waitlist_notifications_logs.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('notid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('dataid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('datatype', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('timesend', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    
	// Adding keys to table waitlist_notifications_logs.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for waitlist_notifications_logs.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    // Notifications
    $notifications = array();
    $notifications[] = array('id'=>1, 
                             'type'=>'user', 
                             'name'=>'User enrolled', 
                             'description'=>'Sends when user was enrolled to course', 
                             'tags'=>'a:7:{i:0;s:14:"recipient name";i:1;s:15:"recipient email";i:2;s:17:"support user name";i:3;s:13:"support email";i:4;s:11:"course name";i:5;s:11:"course link";s:17:"course start date";s:17:"course start date";}', 
                             'subject'=>'Welcome to [course name]', 
                             'body'=>'Hello, [recipient name]<br><br>Welcome to [course name]<br><br>Best reguards,<br>[support user name]<br>', 
                             'sender'=>'supportuser', 
                             'status'=>1, 
                             'role'=>'Student', 
                             'category'=>'Student notifications', 
                             'sortorder	'=>'1');
    
    $notifications[] = array('id'=>2, 
                             'type'=>'user', 
                             'name'=>'User unenrolled', 
                             'description'=>'Sends when user was unenrolled from course', 
                             'tags'=>'a:6:{i:0;s:14:"recipient name";i:1;s:15:"recipient email";i:2;s:17:"support user name";i:3;s:13:"support email";i:4;s:11:"course name";i:5;s:11:"course link";} ', 
                             'subject'=>'You was unenrolled from [course name]', 
                             'body'=>"Hello, [recipient name]<br><br>You was unenrolled from [course name]<br><br>Best reguards,<br>[support user name]<br>", 
                             'sender'=>'supportuser', 
                             'sendto'=>'', 
                             'status'=>1, 
                             'role'=>'Student', 
                             'category'=>'Student notifications', 
                             'sortorder	'=>'2');
    
    $notifications[] = array('id'=>3, 
                             'type'=>'user', 
                             'name'=>'User will be auto unenrolled', 
                             'description'=>'Sending before user will be auto unenrolled', 
                             'tags'=>'a:7:{i:0;s:14:"recipient name";i:1;s:15:"recipient email";i:2;s:17:"support user name";i:3;s:13:"support email";i:4;s:11:"course name";i:5;s:11:"course link";i:6;s:13:"unenroll date";}', 
                             'subject'=>'You will be unenrolled from course [course name]', 
                             'body'=>"Hello, [recipient name]<br><br>You will be unenrolled from course [course name] on [unenroll date]<br><br>Best reguards,<br>[support user name]<br>", 
                             'sender'=>'supportuser', 
                             'sendto'=>'', 
                             'status'=>1,  
                             'role'=>'Student', 
                             'category'=>'Student notifications', 
                             'sortorder	'=>'3');
    
    $notifications[] = array('id'=>4, 
                             'type'=>'user', 
                             'name'=>'User added to waitlist', 
                             'description'=>'Sends when user was added to waitlist', 
                             'tags'=>'a:7:{i:0;s:14:"recipient name";i:1;s:15:"recipient email";i:2;s:17:"support user name";i:3;s:13:"support email";i:4;s:11:"course name";i:5;s:11:"course link";s:17:"course start date";s:17:"course start date";}', 
                             'subject'=>'You was added to waitlist on course [course name]', 
                             'body'=>"Hello, [recipient name]<br><br>You was added to waitlist on course [course name]<br><br>Best reguards,<br>[support user name]<br>", 
                             'sender'=>'supportuser', 
                             'status'=>1,  
                             'role'=>'Student', 
                             'category'=>'Student notifications', 
                             'sortorder	'=>'4');
    
    foreach ($notifications as $notification){
        $new_notification = $notification;
        $record = $DB->get_record('waitlist_notifications', array('id'=>$notification['id']));
        if (!$record){
            unset($new_notification->id);
            $DB->insert_record('waitlist_notifications', $new_notification, false);   
        }
    }
    
    return true;
}
