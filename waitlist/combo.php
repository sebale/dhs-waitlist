<?php
require('../../config.php');
require_once($CFG->libdir. '/coursecatlib.php');

require_once('combo_form.php');
require_once("$CFG->dirroot/enrol/waitlist/library.php");
require_once("$CFG->dirroot/enrol/renderer.php");

$id		  = required_param('id', PARAM_INT);

$course = $DB->get_record('course', array('id'=>$id), '*', MUST_EXIST);
$context = context_course::instance($course->id);

$res = $DB->get_record_sql("select id from ".$CFG->prefix."enrol where courseid=".$id." and enrol='waitlist'");
$instance = $res->id;

if ($course->id == SITEID) {
    redirect(new moodle_url('/'));
}

$return = new moodle_url('/enrol/instances.php', array('id'=>$course->id));
if (!enrol_is_enabled('waitlist')) {
    redirect($return);
}

$records = array(); $crecords = array(); $erecords = array(); $exrecords = array();
$combo_record = $DB->get_record('course_combo_relations', array('courseid'=>$course->id));
if ($combo_record){
    $combo_records = $DB->get_records_sql("SELECT * FROM {course_combo_relations} WHERE combohash = '".$combo_record->combohash."'");
    if (count($combo_records)){
        foreach($combo_records as $record){
            $crecords[$record->courseid] = 1;
        }
    }
    $combo_exists = $DB->get_records_sql("SELECT * FROM {course_combo_relations} WHERE combohash != '".$combo_record->combohash."'");
    if (count($combo_exists)){
        foreach($combo_exists as $record){
            $exrecords[$record->courseid] = 1;
        }
    }
}

$allcategories = coursecat::make_categories_list();
$catcourses = array();
$allcourses = coursecat::get(0)->get_courses(array('recursive' => true));
if (count($allcourses)){
    foreach ($allcourses as $ccourse){
        $catcourses[$ccourse->category][$ccourse->id] = $ccourse;
        if (isset($crecords[$ccourse->id])){
            $records[$ccourse->category][] = $ccourse->id;
        }
        if (isset($exrecords[$ccourse->id])){
            $erecords[$ccourse->category][] = $ccourse->id;
        }
    }
}

require_login($course);
require_capability('enrol/waitlist:config', $context);
$PAGE->set_pagelayout('admin');

$mform = new combo_form(NULL, array($course->id, $allcategories, $catcourses, $records, $erecords));
if ($mform->is_cancelled()) {
    redirect($return);
} else if ($data = $mform->get_data()) {
    $combo_record = $DB->get_record('course_combo_relations', array('courseid'=>$course->id));
    if ($combo_record->combohash){
        $combo_records = $DB->get_records_sql("SELECT * FROM {course_combo_relations} WHERE combohash = '".$combo_record->combohash."'");
        if (count($combo_records)){
            foreach($combo_records as $record){
                $DB->delete_records('course_combo_relations', array('id'=>$record->id));
            }
        }
    }
    $new_hash = md5(time());
    if (count($data->courseids)){
        $new_record = new stdClass();
        $new_record->courseid = $id;
        $new_record->combohash = $new_hash;
        $DB->insert_record('course_combo_relations', $new_record);
        
        foreach($data->courseids as $courses_arr){
            if (count($courses_arr)){
                foreach ($courses_arr as $courseid){
                    $new_record = new stdClass();
                    $new_record->courseid = $courseid;
                    $new_record->combohash = $new_hash;
                    $DB->insert_record('course_combo_relations', $new_record);
                }
            }
        }
    }
    
    redirect($return);
}

$PAGE->set_url('/enrol/waitlist/combo.php', array('id'=>$id));
navigation_node::override_active_url(new moodle_url('/enrol/waitlist/combo.php', array('id' => $id)));

$PAGE->set_title($PAGE->course->fullname);
$PAGE->set_heading($PAGE->title);

echo $OUTPUT->header();

echo $OUTPUT->heading(get_string('combo_courses', 'enrol_waitlist'));

$mform->display();

echo $OUTPUT->footer();

