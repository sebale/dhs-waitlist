<?php
define('AJAX_SCRIPT', true);
require_once('../../config.php');

require_login();

$id		  = optional_param('id', 0, PARAM_INT);
$type	  = optional_param('type', 0, PARAM_INT);
$action = optional_param('action', '', PARAM_RAW);
$form	= (object)clean_param_array($_POST['form'], PARAM_RAW, true);

if($action == 'adminnot-set-status' and $id){
	$status		= optional_param('status', 0, PARAM_INT);
	if($notification = $DB->get_record("waitlist_notifications", array('id'=>$id))){
		$notification->status = $status;
		$DB->update_record("waitlist_notifications", $notification);
	}
	echo time();
} elseif ($action == 'adminnot-save-form'){
	$DB->update_record("waitlist_notifications", $form);
	$notification = $DB->get_record("waitlist_notifications", array('id'=>$form->id));
	echo $notification->id;
	exit;
}	
?>


<?php if ($action == 'adminnot-load-form' and $id) : ?>
	<?php $notification = $DB->get_record("waitlist_notifications", array('id'=>$id)); ?>
	<?php $tag_description = array('recipient name'=>'Recipient name', 'recipient email'=>'Recipient Email', 'support user name'=>'Support User name', 'support email'=>'Support Email', 'course name'=>'Course name', 'course link'=>'Course link', 'course start date'=>'Course start date', 'unenroll date'=>'Unenroll date');?>
    <?php //echo serialize(array('recipient name', 'recipient email', 'support user name', 'support email', 'course name', 'course link', 'unenroll date')); ?>
	<div class="not-form-inner">
		<form name="adminnot-form" class="adminnot-form clearfix" id="adminnot_form_<?php echo $id; ?>" method="POST" action="<?php echo $CFG->wwwroot;?>/enrol/waitlist/notifications_ajax.php">
			<div class="not-form-items-f anot-form">
                <div class="not-form-item">
					<label>Subject:</label><input type="text" name="form[subject]" id="subject" value="<?php echo (isset($notification->subject)) ? $notification->subject : ''; ?>" />
				</div>
				<div class="not-form-item">
					<label>Body:</label><textarea name="form[body]" id="body"><?php echo (isset($notification->body)) ? $notification->body : ''; ?></textarea>
				</div>
				<div class="not-form-item">
					<input type="hidden" name="form[id]" value="<?php echo $id; ?>" />
					<input type="hidden" name="action" value="adminnot-save-form" />
					<button class="send btn btn-success" type="button">Save</button>
					<button class="cancel btn" type="button">Cancel</button>
				</div>
			</div>
			<div class="not-form-items-d">
				<div class="not-form-description">
					<span><strong>Subject</strong> - message subject</span>
					<span><strong>Body</strong> - message body text</span>
					<?php $tags = unserialize($notification->tags); ?>
					<?php if ($tags) : ?>
						<h3>Available Tags:</h3>
						<?php foreach ($tags as $tag) : ?>
							<span><strong>[<?php echo $tag; ?>]</strong> - <?php echo $tag_description[$tag]; ?></span>
						<?php endforeach; ?>
					<?php endif; ?>
                    <?php if (isset($notification->description)) : ?>
                        <br /><span><?php echo $notification->description; ?></span>
                    <?php endif; ?>
				</div>
			</div>
		</form>
	</div>
	<script>
		jQuery("#adminnot_form_<?php echo $id; ?> #body").ClassyEdit();
		jQuery('#adminnot_form_<?php echo $id; ?> .not-form-item .send').click(function(event){
			jQuery.ajax({
				url: jQuery('#adminnot_form_<?php echo $id; ?>').attr('action'),
				type: "POST",
				data: jQuery('#adminnot_form_<?php echo $id; ?>').serialize(),
				beforeSend: function(){
					jQuery('#not_item_<?php echo $id; ?> .not-form-box').html('<i class="fa fa-spinner fa-spin"></i>');
				}
			}).done(function( data ) {
				jQuery('#not_item_<?php echo $id; ?> .not-form-box').addClass('center');
				jQuery('#not_item_<?php echo $id; ?> .not-form-box').html('<i class="fa fa-check green"> <span>Notification successfully saved.</span></i>');
				setTimeout(function() {
					jQuery('#not_item_<?php echo $id; ?> .not-form-box').removeClass('center');
					jQuery("#not_item_<?php echo $id; ?> .load").trigger("click");
				}, 1500);
			});
		});
		jQuery('#adminnot_form_<?php echo $id; ?> .not-form-item .cancel').click(function(event){
			jQuery("#not_item_<?php echo $id; ?> .load").trigger("click");
		});
	</script>
<?php endif; ?>

<?php exit;

